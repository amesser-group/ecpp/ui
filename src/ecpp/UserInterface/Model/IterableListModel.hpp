/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_USERINTERFACE_MODEL_ITERABLELISTMODEL_HPP_
#define ECPP_USERINTERFACE_MODEL_ITERABLELISTMODEL_HPP_

#include <utility>

namespace ecpp::UserInterface::Model
{
  template<class Iterable, class Wrapper>
  class IterableListModel;

  template<class Iterable, class Wrapper>
  class IterableListModelIterator
  {
  protected:
    using IteratorBase = typename Iterable::iterator;

  public:
    constexpr IterableListModelIterator(Iterable& iterable, const IteratorBase& it) : iterable_(iterable), it_(it) {}
    
    Wrapper operator*()  
    { 
      return ((iterable_.begin() <= it_) && (it_ < iterable_.end())) ? Wrapper(*it_) : Wrapper(); 
    }

    IterableListModelIterator & operator++()
    {
      if(it_ < iterable_.end())
        it_++;
      return *this;
    }

    IterableListModelIterator & operator--()
    {
      if(it_ > iterable_.begin())
        it_--;
      return *this;
    }

    constexpr bool operator <(const EndIterationTag&) const { return it_ < iterable_.end(); }

  protected:
    Iterable &   iterable_;
    IteratorBase it_;

    friend class IterableListModel<Iterable, Wrapper>;
  };

  template<class Iterable, class Wrapper>
  class IterableListModel : protected IterableListModelIterator<Iterable, Wrapper>
  {
  protected:
    using Iterator = IterableListModelIterator<Iterable, Wrapper>;

    using Iterator::iterable_;
    using Iterator::it_;
  public:
    using Item = Wrapper;

    IterableListModel(Iterable& iterable) : Iterator(iterable, iterable.begin()) {}

    constexpr Iterator GetSelectedItem() const { return Iterator(iterable_, it_); }

    bool SetSelectedItem(const Iterator &it) 
    { 
      if(it_ == it.it_)
        return false;

      it_ = it.it_; 
      return true;
    }

    constexpr EndIterationTag end() const { return EndIterationTag(); }
  };
}
#endif