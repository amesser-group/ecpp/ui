/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_USERINTERFACE_MODEL_ACTIONLISTITEM_HPP_
#define ECPP_USERINTERFACE_MODEL_ACTIONLISTITEM_HPP_

#include "ecpp/String.hpp"
#include "ecpp/StringEncodings/Unicode.hpp"

namespace ecpp::UserInterface::Text
{
  template<typename FrameBufferAdapter>
  class TextPainterBase;
}

namespace ecpp::UserInterface::Graphic
{
  template<typename BasePainter>
  class Painter;
}

namespace ecpp::UserInterface::Model
{
  template<typename WidgetEnvironment>
  class ActionListItem
  {
  protected:
    using String = ::ecpp::StringPointer<::ecpp::StringEncodings::Utf8>;

  public:
    typedef void (*ActivateCallback)(const typename WidgetEnvironment::Context &ctx);

    constexpr ActionListItem(const String & name, ActivateCallback fn_activate) : name_{name}, fn_activate_ {fn_activate} {}

    template<typename Model>
    void Draw(typename WidgetEnvironment::Painter &painter, const Model&, bool selected) const
    {
      painter.Draw(*this,selected);
    }

    void Activate(const typename WidgetEnvironment::Context &ctx) const
    {
      fn_activate_(ctx);
    }

    constexpr bool IsVisible() const
    {
      return true;
    }

    constexpr String name() const { return name_; }

    static constexpr ActionListItem kDummyItem { "", [] (const typename WidgetEnvironment::Context &) {} };
  protected:
    const String           name_;
    const ActivateCallback fn_activate_;

    template<typename FrameBufferAdapter>
    friend class ecpp::UserInterface::Text::TextPainterBase;

    template<typename BasePainter>
    friend class ecpp::UserInterface::Graphic::Painter;

    friend class WidgetEnvironment::Painter;
  };


  template<typename WidgetEnvironment>
  class FilterableActionListItem : public ActionListItem<WidgetEnvironment>
  {
  protected:
    using typename ActionListItem<WidgetEnvironment>::String;

  public:
    using typename ActionListItem<WidgetEnvironment>::ActivateCallback;
    typedef bool (*IsVisibleCallback)();

    constexpr FilterableActionListItem(const String & name, ActivateCallback fn_activate, IsVisibleCallback fn_visible) : 
      ActionListItem<WidgetEnvironment>{name, fn_activate}, fn_visible_{fn_visible} {}

    constexpr FilterableActionListItem(const String & name, ActivateCallback fn_activate) : 
      ActionListItem<WidgetEnvironment>{name, fn_activate} {}

    bool IsVisible() const
    {
      return fn_visible_();
    }

    static constexpr FilterableActionListItem kDummyItem { "", [] (const typename WidgetEnvironment::Context &) {} };
  protected:
    const IsVisibleCallback fn_visible_ { []() { return true; }};

    friend class WidgetEnvironment::Painter;
  };
}
#endif