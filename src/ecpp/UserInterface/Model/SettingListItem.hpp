/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_USERINTERFACE_MODEL_SETTINGLIST_ITEM_H_
#define ECPP_USERINTERFACE_MODEL_SETTINGLIST_ITEM_H_

#include "ecpp/StringEncodings/Unicode.hpp"

namespace ecpp::UserInterface::Model
{
  template<typename WidgetEnvironment, typename AdjustMode_>
  class SettingListItem
  {
  protected:
    using String = ::ecpp::StringPointer<::ecpp::StringEncodings::Utf8>;

    using Context  = typename WidgetEnvironment::Context;
    using KeyEvent = typename WidgetEnvironment::Event::KeyEvent;
    using Painter  = typename WidgetEnvironment::Painter;

  public:
    using AdjustMode = AdjustMode_;

    typedef void (*DrawCallback)(Painter &painter, const AdjustMode &mode, bool selected);
    typedef void (*UserInputCallback)(KeyEvent user_input, const Context &ctx, AdjustMode &mode);

    constexpr SettingListItem(DrawCallback fn_draw, UserInputCallback fn_userinput) :
      fn_draw_(fn_draw), fn_userinput_(fn_userinput) {}

    template<typename Model>
    void Draw(Painter &painter, const Model &model, bool selected) const
    {
      fn_draw_(painter, model.adjust_mode(), selected);
    }

    template<typename Model>
    void HandleEvent(KeyEvent user_input, const Context &ctx, Model &model) const 
    {
      fn_userinput_(user_input, ctx, model.adjust_mode());
    }

    constexpr void Activate(const Context &ctx) const
    {
    }

    constexpr bool IsVisible() const { return true; }
    
   static const SettingListItem kDummyItem;
  protected:
    const DrawCallback      fn_draw_;
    const UserInputCallback fn_userinput_;
  };

  template<typename WidgetEnvironment, typename AdjustMode_>
  constexpr const SettingListItem<WidgetEnvironment, AdjustMode_> SettingListItem<WidgetEnvironment, AdjustMode_>::kDummyItem {
    [] (Painter &painter, const AdjustMode_&, bool selected) { painter.template DrawTextItem<WidgetEnvironment>("Zurück", selected); },
    [] (KeyEvent user_input, const Context &ctx, AdjustMode_&) { ctx.PopWidget(); }
  };

  template<template<typename> typename ListModel, typename ListItem>
  class SettingListModel : public ListModel<ListItem>
  {
  public:
    using ListModel<ListItem>::ListModel;

    using AdjustMode = typename ListItem::AdjustMode;

    AdjustMode & adjust_mode()             { return adjust_mode_;}
    const AdjustMode & adjust_mode() const { return adjust_mode_;}

  protected:
    AdjustMode adjust_mode_;
  };
  
}
#endif
