/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_USERINTERFACE_MODEL_ARRAYLISTMODEL_HPP_
#define ECPP_USERINTERFACE_MODEL_ARRAYLISTMODEL_HPP_

#include <utility>

namespace ecpp::UserInterface::Model
{
  template<typename Item, typename ItemIndex>
  class ArrayListModel;

  template<typename _Item, typename _ItemIndex>
  class ArrayListModelIterator
  {
  public:
    using Item      = _Item;
    using ItemIndex = _ItemIndex;

    constexpr const Item& operator*() const 
    { 
      return (selected_item_ < num_items_) ? items_[selected_item_] : Item::kDummyItem;
    }

    constexpr const Item* operator->() const 
    { 
      return (selected_item_ < num_items_) ? &(items_[selected_item_]) : &(Item::kDummyItem);
    }

    ArrayListModelIterator& operator--()
    {
      while(selected_item_ > 0)  
      {
        --selected_item_;

        if(operator*().IsVisible())
          break;
      }

      return *this;
    }

    ArrayListModelIterator& operator++()
    {
      while (selected_item_ < num_items_)
      {
        ++selected_item_;

        if(operator*().IsVisible())
          break;
      }
      return *this;
    }

    constexpr bool operator < (const EndIterationTag) const
    {
      return selected_item_ < num_items_;
    }

    constexpr ArrayListModelIterator(const ArrayListModelIterator & init) = default;
  private:
    template<ItemIndex cnt>
    constexpr ArrayListModelIterator(const Item (&items)[cnt]) : items_{items}, num_items_{cnt} {}

    const Item* items_;
    ItemIndex   num_items_;
    ItemIndex   selected_item_ { 0 };

    friend class ArrayListModel<Item, ItemIndex>;
  };

  template<typename _Item, typename _ItemIndex = int>
  class ArrayListModel : protected ArrayListModelIterator<_Item, _ItemIndex>
  {
  protected:
    using ArrayListModelIterator<_Item, _ItemIndex>::num_items_;
    using ArrayListModelIterator<_Item, _ItemIndex>::selected_item_;
  public:
    using typename ArrayListModelIterator<_Item, _ItemIndex>::Item;
    using typename ArrayListModelIterator<_Item, _ItemIndex>::ItemIndex;

    template<ItemIndex cnt>
    constexpr ArrayListModel(const Item (&items)[cnt]) : ArrayListModelIterator<Item, ItemIndex>(items) {}
    
    constexpr ArrayListModelIterator<Item, ItemIndex> GetSelectedItem() const { return {*this}; }
    constexpr auto  GetSelectedIndex() const { return this->selected_item_; }

    constexpr EndIterationTag end() const { return EndIterationTag(); }

    constexpr bool operator==(const ArrayListModel &rhs) const
    {
      return (this->items_ == rhs.items_) && (num_items_ == rhs.num_items_);
    }

    constexpr bool operator!=(const ArrayListModel &rhs) const
    {
      return !(this->operator==(rhs));
    }

    bool SetSelectedItem(const ArrayListModelIterator<Item, ItemIndex> & it) 
    { 
      if (it.selected_item_ >= num_items_)
        return false;

      if (it.selected_item_ == selected_item_)
        return false;

      selected_item_ = it.selected_item_;

      return true;
    }    
  };
}
#endif