/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_USERINTERFACE_MODEL_LISTWIDGET_
#define ECPP_USERINTERFACE_MODEL_LISTWIDGET_

#include "ecpp/String.hpp"
#include "ecpp/StringEncodings/Unicode.hpp"

#include <type_traits>
#include <utility>
#include <cstdint>


namespace ecpp::UserInterface::Text
{
  template<typename FrameBufferAdapter>
  class TextPainterBase;
}

namespace ecpp::UserInterface::Graphic
{
  template<typename BasePainter>
  class Painter;
}

namespace ecpp::UserInterface::Model
{
  template<typename _ListModel, typename WidgetEnvironment>
  class ListWidget : public WidgetEnvironment::Widget
  {
  protected:
    using String = ::ecpp::StringPointer<::ecpp::StringEncodings::Utf8>;

  public:
    using ListModel = _ListModel;

    template<typename ...ARGS>
    constexpr ListWidget(String title, ARGS && ...args) : title_{title}, model_{ ::std::forward<ARGS>(args)...} {}

    /** Draw the window using the provided painter */
    void Draw(typename WidgetEnvironment::Painter &ctx) const override;
    void HandleEvent(const typename WidgetEnvironment::Event &event, const typename WidgetEnvironment::Context &ctx) override;

    constexpr const auto & title() const { return title_; }
  protected:
    void SelectNextItem(const typename WidgetEnvironment::Context &ctx);
    void SelectPrevItem(const typename WidgetEnvironment::Context &ctx);
    
    String         title_;
    ListModel      model_;
    int_least8_t   shift_ {0};

    template<typename FrameBufferAdapter>
    friend class ecpp::UserInterface::Text::TextPainterBase;

    template<typename BasePainter>
    friend class ecpp::UserInterface::Graphic::Painter;

    friend class WidgetEnvironment::Painter;
  };

  template<typename _ListModel, typename WidgetEnvironment>
  void ListWidget<_ListModel, WidgetEnvironment>::Draw(typename WidgetEnvironment::Painter &painter) const
  {
    painter.Draw(*this);
  }

  template<typename _ListModel, typename WidgetEnvironment>
  void ListWidget<_ListModel, WidgetEnvironment>::HandleEvent(const typename WidgetEnvironment::Event &event, const typename WidgetEnvironment::Context &ctx)
  {
    using UserInput = ::std::remove_pointer_t<decltype(event.GetIfUserInput())>;
    const auto *user_input    = event.GetIfUserInput();

    if (user_input == nullptr)
    {
      WidgetEnvironment::Widget::HandleEvent(event, ctx);
      return;
    }

    switch(*user_input)
    {
    case UserInput::kDown:    
      SelectNextItem(ctx); 
      break;
    case UserInput::kUp:      
      SelectPrevItem(ctx); 
      break;
    case UserInput::kClicked:
      (*(model_.GetSelectedItem())).Activate(ctx);
      break;
    default:
      WidgetEnvironment::Widget::HandleEvent(event, ctx);
      break;
    }
  }

  template<typename _ListModel, typename WidgetEnvironment>
  void ListWidget<_ListModel, WidgetEnvironment>::SelectNextItem(const typename WidgetEnvironment::Context &ctx)
  {
    if (model_.SetSelectedItem(++model_.GetSelectedItem()))
    {
      const auto visible_items  = ctx.GetVisibleItemCount(*this);

      if( (shift_ + 1) >= visible_items)
        shift_ = 0;
      else
        shift_++;
    }
  }

  template<typename _ListModel, typename WidgetEnvironment>
  void ListWidget<_ListModel, WidgetEnvironment>::SelectPrevItem(const typename WidgetEnvironment::Context &ctx)
  {
    if (model_.SetSelectedItem(--model_.GetSelectedItem()))
    {
      const auto visible_items  = ctx.GetVisibleItemCount(*this);

      if( shift_ == 0)
        shift_ = (visible_items > 0) ? (visible_items - 1) : 0;
      else
        shift_--;
    }
  }
}

#endif
