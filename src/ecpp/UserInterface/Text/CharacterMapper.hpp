/*
 *  Copyright 2018-2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Musicbox projects firmware
 *
 *  Musicbox's firmware is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Musicbox's firmware is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with musicbox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ECPP_USERINTERFACE_TEXT_HPP_
#define ECPP_USERINTERFACE_TEXT_HPP_

#include "ecpp/StringEncodings/Unicode.hpp"
#include "ecpp/Coordinates.hpp"
#include <cstdint>

namespace ecpp::UserInterface::Text
{
  template<typename FrameBuffer_>
  class CharacterMapper
  {
  protected:
    using FrameBuffer = FrameBuffer_;
    using BufferIndex = typename FrameBuffer::BufferIndex;
    
  public:
    using EditingMode = typename FrameBuffer::EditingMode;

    using Point = typename FrameBuffer::Point;
    using Rect  = typename FrameBuffer::Rect;

    using Encoding      = typename FrameBuffer::DisplayEncoding;
    using Codepoint     = typename Encoding::Codepoint;

    class Character;

    using BufferElement = Character;

    CharacterMapper(const CharacterMapper&) = default;

    constexpr CharacterMapper(FrameBuffer &framebuffer) : fb_(framebuffer) {}

    constexpr Character operator [] (BufferIndex idx) 
    { 
      return {&fb_[idx]}; 
    }

    void SetEditLocation(Point upper_left, Point lower_right, EditingMode mode) const
    {
      fb_.SetEditLocation(upper_left, lower_right, mode);
    }

    void SetEditLocation(Rect rect, EditingMode mode) const
    {
      fb_.SetEditLocation(rect, mode);
    }

    constexpr auto columns() const {return fb_.columns(); }
    constexpr auto rows()    const {return fb_.rows(); }

  protected:
    FrameBuffer &                        fb_;
  };

  template<typename FrameBuffer>
  class CharacterMapper<FrameBuffer>::Character : protected FrameBuffer::DisplayEncoding::Encoder
  {
  private:
    static typename Encoding::BufferElement kDummy;

  public:
    using FrameBuffer::DisplayEncoding::Encoder::operator=;

    constexpr void operator = (char c)
    {
      operator=(::ecpp::StringEncodings::Unicode::Codepoint(c));
    }

    constexpr Character() : FrameBuffer::DisplayEncoding::Encoder{&kDummy} {};
  private:
    constexpr Character(typename Encoding::BufferElement * ptr) : FrameBuffer::DisplayEncoding::Encoder{ptr} {};


    friend class CharacterMapper<FrameBuffer>;
  };

  template<typename FrameBuffer>
  typename CharacterMapper<FrameBuffer>::Encoding::BufferElement CharacterMapper<FrameBuffer>::Character::kDummy;
}
#endif /* ECPP_PERIPHERALS_DISPLAY_CHARACTERMAPPING_HPP_ */
