/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_USERINTERFACE_TEXT_SETTINGLIST_WIDGET_H_
#define ECPP_USERINTERFACE_TEXT_SETTINGLIST_WIDGET_H_

#include "ecpp/UserInterface/Model/SettingListItem.hpp"
#include "ecpp/UserInterface/Model/ListWidget.hpp"

namespace ecpp::UserInterface::Text
{
  template<typename SettingListModel, typename WidgetEnvironment>
  class SettingListWidget : public ::ecpp::UserInterface::Model::ListWidget<SettingListModel, WidgetEnvironment>
  {
  protected:
    using _ListWidget = ::ecpp::UserInterface::Model::ListWidget<SettingListModel, WidgetEnvironment>;
    using AdjustMode = typename SettingListModel::AdjustMode;
  public:
    using _ListWidget::ListWidget;

    void Draw(typename WidgetEnvironment::Painter &ctx) const override;
    void HandleEvent(const typename WidgetEnvironment::Event &event, const typename WidgetEnvironment::Context &ctx) override;

  protected:
    friend typename SettingListModel::Item;
  };
  
  template<typename SettingListModel, typename WidgetEnvironment>
  void SettingListWidget<SettingListModel, WidgetEnvironment>::Draw(typename WidgetEnvironment::Painter &painter) const
  {
    painter.Draw(*this);
  }

  template<typename SettingListModel, typename WidgetEnvironment>
  void SettingListWidget<SettingListModel, WidgetEnvironment>::HandleEvent(const typename WidgetEnvironment::Event &event, const typename WidgetEnvironment::Context &ctx)
  {
    using UserInput = ::std::remove_pointer_t<decltype(event.GetIfUserInput())>;
    const auto *user_input    = event.GetIfUserInput();

    if (user_input == nullptr)
    {
      WidgetEnvironment::Widget::HandleEvent(event, ctx);
      return;
    }

    switch(*user_input)
    {
    case UserInput::kDown:    
      if (_ListWidget::model_.adjust_mode() == AdjustMode::kTopLevel)
        _ListWidget::SelectNextItem(ctx); 
      else
        this->model_.GetSelectedItem()->HandleEvent(*user_input, ctx, _ListWidget::model_);
      break;
    case UserInput::kUp: 
      if (_ListWidget::model_.adjust_mode() == AdjustMode::kTopLevel)
        _ListWidget::SelectPrevItem(ctx); 
      else
        this->model_.GetSelectedItem()->HandleEvent(*user_input, ctx, _ListWidget::model_);
      break;
    case UserInput::kClicked:
      this->model_.GetSelectedItem()->HandleEvent(*user_input, ctx, _ListWidget::model_);
      break;
    default:
      WidgetEnvironment::Widget::HandleEvent(event, ctx);
      break;
    }
  }

}

#endif