/*
 *  Copyright 2020, 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_USERINTERFACE_TEXT_PAINTER_H_
#define ECPP_USERINTERFACE_TEXT_PAINTER_H_

#include "ecpp/UserInterface/Model/ListWidget.hpp"
#include "ecpp/UserInterface/Model/ActionListItem.hpp"

#include "ecpp/Datatypes.hpp"
#include "ecpp/Math/Fraction.hpp"
#include "ecpp/String.hpp"
#include "ecpp/StringEncodings/Unicode.hpp"

#include <stdio.h>
#include <cstdarg>
#include <cstdint>
#include <cstring>

namespace ecpp::UserInterface::Text
{
  template<typename Val>
  constexpr Val saturated_sub(Val lhs, Val rhs) { return (lhs > rhs) ? (lhs - rhs) : 0; }

  using ::std::strlen;
  using ::std::memcpy;
  using ::std::int_least8_t;

  template<typename FrameBufferAdapter>
  class TextPainterBase : private FrameBufferAdapter
  {
  protected:
    using typename FrameBufferAdapter::BufferIndex;
    using typename FrameBufferAdapter::BufferElement;

    using StringPointer = ::ecpp::StringPointer<::ecpp::StringEncodings::Utf8>;
    using StringView    = ::ecpp::StringView<::ecpp::StringEncodings::Utf8>;

    static constexpr BufferElement kOutOfArea {};
  public:
    using typename FrameBufferAdapter::EditingMode;
    using typename FrameBufferAdapter::Point;
    using typename FrameBufferAdapter::Rect;
    using typename FrameBufferAdapter::Codepoint;
    
    typedef size_t                   TextLenType;

    constexpr TextPainterBase(const TextPainterBase& init) :
      FrameBufferAdapter(init), offset_(init.offset_), columns_(init.columns_), rows_(init.rows_)
    {}

    constexpr TextPainterBase(const TextPainterBase & base, Point upper_left, Point lower_right) :
      FrameBufferAdapter(base), 
      offset_(base.offset_ + upper_left), 
      columns_(saturated_sub(lower_right.x,upper_left.x)),
      rows_(saturated_sub(lower_right.y,upper_left.y))
    {}

    constexpr TextPainterBase(const TextPainterBase & base, Rect rect) :
      TextPainterBase(base, rect.upper_left, rect.lower_right) {}

    explicit constexpr TextPainterBase(const FrameBufferAdapter & init) :  
      FrameBufferAdapter{init}, offset_(Point(0,0)), columns_(init.columns()), rows_(init.rows())  {}

    constexpr TextPainterBase GetSubArea(Rect rect) const
    {
      return TextPainterBase(*this, rect);
    }

    constexpr TextPainterBase CreateField(Rect rect) const
    {
      return TextPainterBase(*this, rect);
    }

    constexpr auto columns() const { return columns_; }
    constexpr auto rows()    const { return rows_; }

    constexpr auto width()   const { return columns_; }
    constexpr auto height()  const { return rows_; }

    constexpr BufferElement operator [] (Point loc) 
    { 
      return (loc.x < columns_ && loc.y < rows_) ? FrameBufferAdapter::operator[](offset_ + loc) : kOutOfArea; 
    }

    void SetEditLocation(Point upper_left, Point lower_right, EditingMode mode) const
    {
      FrameBufferAdapter::SetEditLocation(offset_ + upper_left, offset_ + lower_right, mode);
    }

    void SetEditLocation(Rect rect, EditingMode mode) const
    {
      SetEditLocation(rect.upper_left, rect.lower_right, mode);
    }

    void Clear();
    void Fill(Codepoint c);

    void PutText(StringPointer string);
    void PutText(StringView string);

    void CenterText(StringPointer string);
    void CenterText(StringView    string);

    void IPrintF(const char* format, ...);

    template<typename WidgetEnvironment>
    void DrawTextItem(StringPointer string, bool selected);

    template<typename WidgetEnvironment>
    void DrawTextItem(StringView string, bool selected);

    template<typename T>
    void DrawProgress(const ::ecpp::Math::Fraction<T> fraction);

    template<typename ListModel, typename WidgetEnvironment>
    void Draw(const Model::ListWidget<ListModel, WidgetEnvironment> & list_widget);

    template <typename WidgetEnvironment>
    void Draw(const Model::ActionListItem<WidgetEnvironment> & action_list_item, bool selected);

    template<typename ListModel, typename WidgetEnvironment>
    constexpr static auto GetVisibleItemCount(const Rect & rect, const Model::ListWidget<ListModel, WidgetEnvironment> & list_widget)
    {
      return (rect.GetHeight() > 1) ? (rect.GetHeight() - 1) : rect.GetHeight();
    }
  protected:
    const BufferIndex                    offset_;
    /** number of columns */
    const decltype(Point::x)             columns_;
    /** number of rows */
    const decltype(Point::y)             rows_;
  };

  
  template<typename FrameBufferAdapter>
  void TextPainterBase<FrameBufferAdapter>::Clear()
  {
    Fill(::ecpp::StringEncodings::Unicode::Codepoint(' '));
  }

  template<typename FrameBufferAdapter>
  void TextPainterBase<FrameBufferAdapter>::Fill(TextPainterBase<FrameBufferAdapter>::Codepoint cp)
  {
    Point l(0,0);

    for(l.y = 0; l.y < height(); l.y++)
    {
      for (l.x = 0; l.x < width(); l.x++)
      {
        FrameBufferAdapter::operator[](l) = cp;
      }
    }
  }

  template<typename FrameBufferAdapter>
  void TextPainterBase<FrameBufferAdapter>::PutText(StringPointer string)
  {
    Point pos {0,0};

    for(auto it = string.begin(); it < string.end(); ++it, ++pos.x)
      operator[](pos) = *it;
  }

  template<typename FrameBufferAdapter>
  void TextPainterBase<FrameBufferAdapter>::PutText(StringView string)
  {
    Point pos {0,0};

    for(auto it = string.begin(); it < string.end(); ++it, ++pos.x)
      operator[](pos) = *it;
  }


  template<typename FrameBufferAdapter>
  void TextPainterBase<FrameBufferAdapter>::CenterText(StringPointer string)
  {
    auto w = string.CountCharacters();

    if (w < width())
      CreateField( {(width() - w)/ 2, 0, (width() + w + 1)/2,1}).PutText(string);
    else
      PutText( string );
  }

  template<typename FrameBufferAdapter>
  void TextPainterBase<FrameBufferAdapter>::CenterText(StringView string)
  {
    auto w = string.CountCharacters();

    if (w < width())
      CreateField( {(width() - w)/ 2, 0, (width() + w + 1)/2,1}).PutText(string);
    else
      PutText( string );
  }

  template<typename FrameBufferAdapter>
  void TextPainterBase<FrameBufferAdapter>::IPrintF(const char* format, ...)
  {
    char buffer[width() * 4 + 1];
    ::std::va_list args;

    va_start(args, format);
    vsniprintf(buffer, sizeof(buffer) - 1, format, args);
    va_end(args);

    buffer[sizeof(buffer)-1] = '\0';

    PutText(StringPointer(buffer));
  }

  template<typename FrameBufferAdapter>
  template<typename WidgetEnvironment>
  void TextPainterBase<FrameBufferAdapter>::DrawTextItem(StringPointer string, bool selected)
  {
    CreateField({1, 0, width() - 1, 1}).PutText(string);

    if (selected)
    {
      (*this)[{0,0}] = '[';
      (*this)[{width()-1, 0}] = ']';
    }
  }

  template<typename FrameBufferAdapter>
  template<typename WidgetEnvironment>
  void TextPainterBase<FrameBufferAdapter>::DrawTextItem(StringView string, bool selected)
  {
    CreateField({1, 0, width() - 1, 1}).PutText(string);

    if (selected)
    {
      (*this)[{0,0}] = '[';
      (*this)[{width()-1, 0}] = ']';
    }
  }


  template<typename FrameBufferAdapter>
  template<typename T>
  void TextPainterBase<FrameBufferAdapter>::DrawProgress(const ::ecpp::Math::Fraction<T> fraction)
  {
    if (0 == fraction.denom)
    {
      Fill(::ecpp::StringEncodings::Unicode::Codepoint('-'));
    }
    else
    {
      Point pos{0,0};
      auto n = width();

      if(fraction.num < fraction.denom)
        n = n * fraction.num / fraction.denom;

      for(;pos.x < n; ++pos.x)
        (*this)[pos] = '#';
    }
  }


  template<typename FrameBufferAdapter>
  class TextPainterRow : public TextPainterBase<FrameBufferAdapter>
  {
  public:
    using typename TextPainterBase<FrameBufferAdapter>::Point;
    using typename TextPainterBase<FrameBufferAdapter>::Rect;

    TextPainterRow(const TextPainterRow&)  = default;
    using TextPainterBase<FrameBufferAdapter>::TextPainterBase;

    constexpr TextPainterRow CreateField(Rect rect) {return TextPainterRow(*this, rect); }

    using TextPainterBase<FrameBufferAdapter>::operator[];

    constexpr auto operator[] (decltype(Point::x) x)
    {
      return TextPainterBase<FrameBufferAdapter>::operator[](Point(x, 0));
    }
  };

  template<typename FrameBufferAdapter>
  class TextPainter : public TextPainterBase<FrameBufferAdapter>
  {
  public:
    using typename TextPainterBase<FrameBufferAdapter>::Point;
    using typename TextPainterBase<FrameBufferAdapter>::Rect;

    TextPainter(const TextPainter &init) = default;

    constexpr TextPainter(const TextPainterBase<FrameBufferAdapter> &init) :
      TextPainterBase<FrameBufferAdapter>(init) {}

    using TextPainterBase<FrameBufferAdapter>::TextPainterBase;

    using TextPainterBase<FrameBufferAdapter>::width;

    constexpr TextPainter CreateField(Rect rect)  { return TextPainter(*this, rect);  }

    using TextPainterBase<FrameBufferAdapter>::operator[];
    
    constexpr TextPainterRow<FrameBufferAdapter> operator[] (decltype(Point::y) y)
    {
      return {*this, {0,y,width(),y+1} };
    }
  };

#if 0
  template<typename TextPainterBase>
  void TextPainter<TextPainterBase>::putChar(const char text, Location loc)
  {
    if( (loc.col >= num_col()) || (loc.row >= num_row()))
      return;

    (*this)[loc] = text;
  }

  template<typename TextPainterBase>
  void TextPainter<TextPainterBase>::putText(const char* text)
  {
    putText(text, strlen(text));
  }

  template<typename TextPainterBase>
  void TextPainter<TextPainterBase>::putText(const char* text, size_t len)
  {
    putText( {text, len}, {0,0});
  }

  template<typename TextPainterBase>
  void TextPainter<TextPainterBase>::putText(const char* text, size_t len, Column offset)
  {
    putText( {text, len}, { offset, 0 });
  }

  template<typename TextPainterBase>
  void TextPainter<TextPainterBase>::putText(TextSpan text, Location loc)
  {
    auto it = text.begin();

    if(loc.col >= this->num_col())
      return;

    while(loc.row < this->num_row())
    {
      /* manual copy of text till terminating 0 */
      for(; loc.col < this->num_col(); ++loc.col, ++it)
      {
        auto cp = *it;

        if (cp.kStringEnd() == cp)
          break;
        else
          (*this)[loc] = cp;
      }

      for(; loc.col < this->num_col(); ++loc.col)
        (*this)[loc] = ' ';

      loc.row++;
      loc.col = 0;
    }
  }

  template<typename TextPainterBase>
  void TextPainter<TextPainterBase>::centerText(const char *text)
  {
    centerText(TextSpan(text));
  }

  template<typename TextPainterBase>
  void TextPainter<TextPainterBase>::centerText(TextSpan text)
  {
    Location l(0,0);
    auto it = text.begin();
    auto c  = text.countCharacters();

    if (c < num_col())
      l.col = (num_col() - c) / 2;

    for(; (l.col < num_col()) && (it < text.end()); ++l.col, ++it)
      (*this)[l] = *it;
  }


  template<typename TextPainterBase>
  void TextPainter<TextPainterBase>::iprintf(const char* format, ...)
  {
    char buffer[this->num_col()*2];
    ::std::va_list args;

    va_start(args, format);
    vsniprintf(buffer, sizeof(buffer), format, args);
    va_end(args);

    putText(buffer);
  }
#endif

  template<typename FrameBufferAdapter>
  template<typename ListModel, typename WidgetEnvironment>
  void TextPainterBase<FrameBufferAdapter>::Draw(const Model::ListWidget<ListModel, WidgetEnvironment> & list_widget)
  {
    auto selection = list_widget.model_.GetSelectedItem();
    const auto offset = (rows() > 1) ? 1 : 0;
    const auto visible_items = rows() - 1;

    if(offset > 0)
      CreateField({0, 0, columns(), 1}).CenterText(list_widget.title_);

    {
      typename WidgetEnvironment::Painter field_painter = CreateField({0, offset + list_widget.shift_, columns(), offset + list_widget.shift_ + 1});
      (*selection).Draw(field_painter, list_widget.model_, true);
    }

    for(auto it = selection, i = list_widget.shift_ - 1; i >= 0; --i)
    {
      typename WidgetEnvironment::Painter field_painter = CreateField({0, offset + i, columns(), offset + i + 1});
      (*(--it)).Draw(field_painter, list_widget.model_, false);
    }

    for(auto it = selection, i = list_widget.shift_ + 1; (i < visible_items) && (it < list_widget.model_.end()); ++i)
    {
      typename WidgetEnvironment::Painter field_painter = CreateField({0, offset + i, columns(), offset + i + 1});
      (*(++it)).Draw(field_painter, list_widget.model_, false);
    }
  }

  template<typename FrameBufferAdapter>
  template <typename WidgetEnvironment>
  void TextPainterBase<FrameBufferAdapter>::Draw(const Model::ActionListItem<WidgetEnvironment> & action_item, bool selected)
  {
    DrawTextItem<WidgetEnvironment>(action_item.name_, selected);
  }

};
#endif /* ECPP_UI_TEXT_PAINTER_H_ */
