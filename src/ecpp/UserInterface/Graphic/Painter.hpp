/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_USERINTERFACE_GRAPHIC_PAINTER_HPP_
#define ECPP_USERINTERFACE_GRAPHIC_PAINTER_HPP_

#include "ecpp/Graphics/Painter.hpp"
#include "ecpp/UserInterface/Model/ListWidget.hpp"
#include "ecpp/UserInterface/Model/ActionListItem.hpp"

namespace ecpp::UserInterface::Graphic
{
  template<typename FrameBufferAdapter>
  class Painter : public ::ecpp::Graphics::Painter<FrameBufferAdapter>
  {
  public:
    enum EditingMode : uint_least8_t
    {
      kEditingModeNone      = 0,
      kEditingModeSelected  = 1,
      kEditingModeAdjusting = 2,
    };

    using Rect  = typename FrameBufferAdapter::Rect;
    using Point = typename FrameBufferAdapter::Point;

    using StringPointer = ::ecpp::StringPointer<::ecpp::StringEncodings::Utf8>;
    using StringView    = ::ecpp::StringView<::ecpp::StringEncodings::Utf8>;

    using ::ecpp::Graphics::Painter<FrameBufferAdapter>::Painter;

    constexpr Painter(const ::ecpp::Graphics::Painter<FrameBufferAdapter> &init) :
      ::ecpp::Graphics::Painter<FrameBufferAdapter>{init} {}

    using ::ecpp::Graphics::Painter<FrameBufferAdapter>::GetHeight;
    using ::ecpp::Graphics::Painter<FrameBufferAdapter>::GetWidth;
    using ::ecpp::Graphics::Painter<FrameBufferAdapter>::height;
    using ::ecpp::Graphics::Painter<FrameBufferAdapter>::width;

    using ::ecpp::Graphics::Painter<FrameBufferAdapter>::Invert;

    constexpr Painter CreateField(Rect rect)
    {
      return ::ecpp::Graphics::Painter<FrameBufferAdapter>::CreateField(rect);
    }

    template<typename WidgetEnvironment, typename Font = typename WidgetEnvironment::template Font<Painter>>
    void DrawTextItem(StringPointer string, bool selected);

    template<typename WidgetEnvironment, typename Font = typename WidgetEnvironment::template Font<Painter>>
    void DrawTextItem(StringView    string, bool selected);

    template<typename ListModel, typename WidgetEnvironment>
    void DrawTitle(const Model::ListWidget<ListModel, WidgetEnvironment> & widget);

    template<typename ListModel, typename WidgetEnvironment>
    void DrawItems(const Model::ListWidget<ListModel, WidgetEnvironment> & list_widget);

    template<typename ListModel, typename WidgetEnvironment>
    void Draw(const Model::ListWidget<ListModel, WidgetEnvironment> & list_widget);

    template <typename WidgetEnvironment>
    void Draw(const Model::ActionListItem<WidgetEnvironment> & action_list_item, bool selected);

    template<typename ListModel, typename WidgetEnvironment>
    constexpr static auto GetVisibleItemCount(const Rect & rect, const Model::ListWidget<ListModel, WidgetEnvironment> & list_widget)
    {
      using ListWidget = typename Model::ListWidget<ListModel, WidgetEnvironment>;
      using Item       = typename ListModel::Item;

      const auto height       = rect.GetHeight();
      const auto item_height  = WidgetEnvironment::template Font<Item>::GetHeight();
      const auto title_height = WidgetEnvironment::template Font<ListWidget>::GetHeight();
      
      if((item_height + title_height) <= height)
      {
        return (height - title_height) / item_height;
      }
      else
      {
        return height / item_height;
      }
    }

    void SetEditLocation(Point upper_left, Point lower_right, EditingMode mode);

    void SetEditLocation(Rect rect, EditingMode mode) {SetEditLocation(rect.upper_left, rect.lower_right, mode);} 

  protected:
    template<typename Font>
    constexpr auto CreateFontRenderer() { return ::ecpp::Graphics::Painter<FrameBufferAdapter>::template CreateFontRenderer<Font>(); }
  };

  template<typename FrameBufferAdapter>
  template<typename WidgetEnvironment, typename Font>
  void Painter<FrameBufferAdapter>::DrawTextItem(StringPointer string, bool selected)
  {
    CreateField({1, 0, GetWidth() - 1, GetHeight()}).template PutText<Font>(string);

    if (selected)
      Invert();
  }

  template<typename FrameBufferAdapter>
  template<typename WidgetEnvironment, typename Font>
  void Painter<FrameBufferAdapter>::DrawTextItem(StringView string, bool selected)
  {
    CreateField({1, 0, GetWidth() - 1, GetHeight()}).template PutText<Font>(string);

    if (selected)
      Invert();
  }

  template<typename FrameBufferAdapter>
  template<typename ListModel, typename WidgetEnvironment>
  void Painter<FrameBufferAdapter>::DrawTitle(const Model::ListWidget<ListModel, WidgetEnvironment> & widget)
  {
    using Widget = const Model::ListWidget<ListModel, WidgetEnvironment>;
    using Font   = typename WidgetEnvironment::template Font<Widget>;
    this->template CenterText<Font>(widget.title());
  }

  template<typename FrameBufferAdapter>
  template<typename ListModel, typename WidgetEnvironment>
  void Painter<FrameBufferAdapter>::DrawItems(const Model::ListWidget<ListModel, WidgetEnvironment> & list_widget)
  {
    using Item       = typename ListModel::Item;

    auto selection = list_widget.model_.GetSelectedItem();

    const auto height       = GetHeight();
    const auto item_height  = WidgetEnvironment::template Font<Item>::GetHeight();
    const auto visible_items = height / item_height;
    
    /* draw selected item */
    { 
      typename WidgetEnvironment::Painter field_painter = CreateField({0, item_height * list_widget.shift_, GetWidth(), item_height * (list_widget.shift_ + 1)});
      (*selection).Draw(field_painter, list_widget.model_, true);
    }

    /* draw items visible before selected item */
    for(auto it = selection, i = list_widget.shift_ - 1; i >= 0; --i)
    { 
      typename WidgetEnvironment::Painter field_painter = CreateField({0, item_height * i, GetWidth(), item_height * (i + 1)});
      (*(--it)).Draw(field_painter, list_widget.model_, false);
    }

    /* draw items visible after selected item */
    for(auto it = selection, i = list_widget.shift_ + 1; (i < visible_items) && (it < list_widget.model_.end()); ++i)
    {
      typename WidgetEnvironment::Painter field_painter = CreateField({0, item_height * i, GetWidth(), item_height * (i + 1)});
      (*(++it)).Draw(field_painter, list_widget.model_, false);
    }
  }

  template<typename FrameBufferAdapter>
  template<typename ListModel, typename WidgetEnvironment>
  void Painter<FrameBufferAdapter>::Draw(const Model::ListWidget<ListModel, WidgetEnvironment> & list_widget)
  {
    using ListWidget = typename Model::ListWidget<ListModel, WidgetEnvironment>;
    using Item       = typename ListModel::Item;

    const auto height       = GetHeight();
    const auto item_height  = WidgetEnvironment::template Font<Item>::GetHeight();
    const auto title_height = WidgetEnvironment::template Font<ListWidget>::GetHeight();
    
    if((item_height + title_height) <= height)
    {
      CreateField({0, 0, GetWidth(), title_height}).DrawTitle(list_widget);
      CreateField({0, title_height, GetWidth(), height}).DrawItems(list_widget);
    }
    else
    {
      this->DrawItems(list_widget);
    }
  }

  template<typename FrameBufferAdapter>
  template <typename WidgetEnvironment>
  void Painter<FrameBufferAdapter>::Draw(const Model::ActionListItem<WidgetEnvironment> & action_item, bool selected)
  {
    using Item = Model::ActionListItem<WidgetEnvironment>;

    DrawTextItem<WidgetEnvironment, typename  WidgetEnvironment::template Font<Item> >(action_item.name_, selected);
  }

  template<typename FrameBufferAdapter>
  void Painter<FrameBufferAdapter>::SetEditLocation(Point upper_left, Point lower_right, EditingMode mode)
  {
    if(mode != kEditingModeNone)
    {
      CreateField({upper_left.x,upper_left.y, lower_right.x, lower_right.y}).Invert();

      if(mode == kEditingModeSelected)
        CreateField({upper_left.x+1,upper_left.y+1, lower_right.x-1, lower_right.y-1}).Invert();
    }      
  }
}
#endif
