/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_DRIVERS_COMMON_CHARACTERDISPLAY_HPP_
#define ECPP_DRIVERS_COMMON_CHARACTERDISPLAY_HPP_

#include <cstdint>

namespace ecpp::Drivers::Common
{
  struct CharacterDisplayLocation
  {
    uint_least8_t x {0};
    uint_least8_t y {0};

    constexpr CharacterDisplayLocation(int x) : x(static_cast<uint_least8_t>(x)) {};
    constexpr CharacterDisplayLocation(int x, int y) : x(static_cast<uint_least8_t>(x)), y(static_cast<uint_least8_t>(y)) {};

    constexpr bool operator!= (const CharacterDisplayLocation& rhs) const { return (x != rhs.x) || (y != rhs.y); }
  };

  class CharacterDisplay
  {
  public:
    enum CursorMode : uint_least8_t
    {
      kCursorOff       = 0x00,
      kCursorBlink     = 0x01,
      kCursorUnderline = 0x02,
    };  
  };
}

#endif