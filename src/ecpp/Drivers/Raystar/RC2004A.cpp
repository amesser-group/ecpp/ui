/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#include "ecpp/Drivers/Raystar/RC2004A.hpp"

#include "ecpp/System/TickTimer.hpp"
#include "ecpp/Units/Time.hpp"

#include "System.hpp"

using namespace ecpp::Drivers::Display::RC2004A;
using namespace ecpp::Target;

using ::ecpp::System::TickTimer;
using ::ecpp::Units::MilliSecond;

uint8_t
Codepoint::Convert(::ecpp::StringEncodings::Unicode::Codepoint src)
{
  auto cp = src.GetRaw();

  if(cp == 0)
    return 0x20;
  else if (cp >= 0x20 && cp <= 0x5B)
    return cp;
  else if (cp >= 0x5D && cp <= 0x7D)
    return cp;

  switch(cp)
  {
  case U'Ä': return 'A';
  case U'ä': return 'a';
  case U'ß': return 's';
  case U'µ': return 'u';
  case U'ö': return 'o';
  case U'Ö': return 'O';
  case U'ü': return 'u';
  case U'Ü': return 'U';

  default: return kMappingFailed;
  }
}

void
Parallel4Bit::Init()
{
  TickTimer tmr;

  tmr.Start();
  Bsp::ResetDisplay(*this);
  while(not tmr.IsExpired(MilliSecond<>(50)));


  Bsp::clearLcdRS(*this);

  /* Switch over to 8bit mode in any case */
  Bsp::writeLcdNibble(*this, 0x3);
  Bsp::delay(MilliSecond<>(1));
  Bsp::writeLcdNibble(*this, 0x3);
  Bsp::delay(MilliSecond<>(1));
  Bsp::writeLcdNibble(*this, 0x3);
  Bsp::delay(MilliSecond<>(1));
  Bsp::writeLcdNibble(*this, 0x3);
  Bsp::delay(MilliSecond<>(1));

  /* now switch to four bit */
  Bsp::writeLcdNibble(*this, 0x2);
  Bsp::delay(MilliSecond<>(1));

  /* all following writes will be in 4 bit mode */

  /* function set */
  sendCommand(0x28);

  /* display off */
  sendCommand(0x08);

  /* entry mode */
  sendCommand(0x06);

  /* display home */
  sendCommand(0x02);

  /* display clear */
  sendCommand(0x01);

  /* display on */
  sendCommand(0x0C);
}

void
Parallel4Bit::SetCursorMode(CursorMode mode)
{
  sendCommand(0xC | mode);
}

void
Parallel4Bit::LocateCursor(uint8_t col, uint8_t row)
{
  Bsp::clearLcdRS(*this);

  switch(row)
  {
  case 0: sendCommand(0x80 + 0x00 + col); break;
  case 1: sendCommand(0x80 + 0x40 + col); break;
  case 2: sendCommand(0x80 + 0x14 + col); break;
  case 3: sendCommand(0x80 + 0x54 + col); break;
  }
}

void
Parallel4Bit::WriteDDRAM(const void* b, uint8_t len)
{
  const uint8_t *p, *pend;

  Bsp::setLcdRS(*this);

  for( p = reinterpret_cast<const uint8_t*>(b), pend = p + len;
       p < pend; ++p)
  {
    if(*p == 0) /* write space instead of null char */
      Bsp::writeLcdByte(*this, 0x20);
    else
      Bsp::writeLcdByte(*this, *p);
  }
}

void
Parallel4Bit::sendCommand(uint8_t command)
{
  Bsp::clearLcdRS(*this);
  Bsp::writeLcdByte(*this, command);
  Bsp::delay(MilliSecond<>(2));
}