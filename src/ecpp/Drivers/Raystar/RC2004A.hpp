/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_DRIVERS_RAYSTAR_RC2004A_HPP_
#define ECPP_DRIVERS_RAYSTAR_RC2004A_HPP_

#include "ecpp/Drivers/Common/CharacterDisplay.hpp"
#include "ecpp/Target/Bsp.hpp"
#include "ecpp/StringEncodings/Unicode.hpp"

namespace ecpp::Drivers::Display::RC2004A
{
  using namespace ::std;

  class Codepoint : public ::ecpp::StringEncoding::Codepoint
  {
  public:
    constexpr static uint8_t kMappingFailed = 0xCD;

    constexpr Codepoint(const Codepoint & init) : val_{init.val_} {}

    template<typename FromCodePoint>
    constexpr Codepoint(FromCodePoint init ) : val_{Convert(init)} {}

    constexpr uint8_t GetRaw() const { return val_; }
  protected:
    static uint8_t Convert(::ecpp::StringEncodings::Unicode::Codepoint src);

    uint_least8_t val_;
  };

  class Parallel4Bit : public ::ecpp::Target::Bsp::DisplayDriver, public Common::CharacterDisplay
  {
  public:
    void SetCursorMode(CursorMode mode);
    void LocateCursor(uint8_t col, uint8_t row);
    void WriteDDRAM(const void* b, uint8_t len);
  protected:
    void Init();
    void sendCommand(uint8_t cmd);
  };

  template<typename BusDriver>
  class Driver : public BusDriver
  {
  public:
    static constexpr uint8_t kDisplayWidth  = 20;
    static constexpr uint8_t kDisplayHeight = 4;

    using Point = ::ecpp::Drivers::Common::CharacterDisplayLocation;
    
    using DisplayEncoding = ::ecpp::OneToOneStringEncoding<uint8_t, Codepoint>;

    void Init() { BusDriver::Init();}
  };
}

#endif