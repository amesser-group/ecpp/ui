/*
 *  Copyright 2020 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#include "ecpp/Drivers/NHD/NHD_0420DZW.hpp"
#include "ecpp/Units/Time.hpp"

using namespace ecpp::Drivers::NHD0420DZW;
using namespace ecpp::Target;

using ::ecpp::Units::MilliSecond;

template<> uint8_t
Codepoint<0>::Convert(::ecpp::StringEncodings::Unicode::Codepoint src)
{
  auto cp = src.GetRaw();

  if(cp == 0)
    return 0x20;
  else if (cp >= 0x20 && cp <= 0x5B)
    return cp;
  else if (cp >= 0x5D && cp <= 0x7D)
    return cp;
  
  return kMappingFailed;
}

template<> uint8_t 
Codepoint<1>::Convert(::ecpp::StringEncodings::Unicode::Codepoint src)
{
  auto cp = src.GetRaw();

  if(cp == 0)
    return 0x20;
  else if (cp >= 0x20 && cp <= 0x5B)
    return cp;
  else if (cp >= 0x5D && cp <= 0x7D)
    return cp;

  switch(cp)
  {
  case U'Û': return 0x80;
  case U'Ù': return 0x81;
  case U'Ú': return 0x82;
  case U'ü':
  case U'Ü': return 0x83;
  case U'û': return 0x84;
  case U'ù': return 0x85;
  case U'ú': return 0x86;
  case U'Ô': return 0x87;
  case U'Ò': return 0x88;
  case U'Ó': return 0x89;

  case U'ô': return 0x8B;
  case U'ò': return 0x8C;
  case U'ó': return 0x8D;
  case U'ö':
  case U'Ö': return 0x8E;
  case U'¿': return 0x8F;
  case U'Ê': return 0x90;
  case U'È': return 0x91;
  case U'É': return 0x92;
  case U'Ë': return 0x93;
  case U'ê': return 0x94;
  case U'è': return 0x95;
  case U'é': return 0x96;
  case U'ë': return 0x97;
  case U'Ȧ': return 0x98;
  case U'Ä': return 0x99;
  case U'ȧ': return 0x9A;
  case U'â': return 0x9B;
  case U'à': return 0x9C;
  case U'á':
  case U'Á': return 0x9D;
  case U'ä': return 0x9E;


  case U'î': return 0xA1;
  case U'ì': return 0xA2;
  case U'í': return 0xA3;
  case U'ï': return 0xA4;
  case U'İ': return 0xA5;
  case U'Ñ': return 0xA6;
  case U'ñ': return 0xA7;

  case U'§': return 0xB0;

  case U'\\': return 0xDA;

  case U'ß': return 0xC3;
  case U'¤': return 0xC6;
  case U'µ': return 0xC8;
  case U'Õ': return 0xCE;

  case U'β': return 0xF5;

  default: return kMappingFailed;
  }
}

void
Parallel4Bit::Init(uint_fast8_t font_table)
{
  Bsp::ResetDisplay(*this);
  Bsp::clearLcdRS(*this);

  /* switch display controller to 4 bit */
  Bsp::writeLcdNibble(*this, 0x2);
  Bsp::writeLcdNibble(*this, 0x2);
  Bsp::writeLcdNibble(*this, 0x8);

  Bsp::delay(MilliSecond<>(1));

  /* function set */
  sendCommand(0x28 | font_table);

  /* display off */
  sendCommand(0x08);

  /* display clear */
  sendCommand(0x01);

  /* entry mode */
  sendCommand(0x06);

  /* home */
  sendCommand(0x02);

  /* display on */
  sendCommand(0x0C);
}

void
Parallel4Bit::SetCursorMode(CursorMode mode)
{
  sendCommand(0xC | mode);
}


void
Parallel4Bit::LocateCursor(uint8_t col, uint8_t row)
{
  Bsp::clearLcdRS(*this);

  switch(row)
  {
  case 0: sendCommand(0x80 + 0x00 + col); break;
  case 1: sendCommand(0x80 + 0x40 + col); break;
  case 2: sendCommand(0x80 + 0x14 + col); break;
  case 3: sendCommand(0x80 + 0x54 + col); break;
  }
}

void
Parallel4Bit::WriteDDRAM(const void* b, uint8_t len)
{
  const uint8_t *p, *pend;

  Bsp::setLcdRS(*this);

  for( p = reinterpret_cast<const uint8_t*>(b), pend = p + len;
       p < pend; ++p)
  {
    if(*p == 0) /* write space instead of null char */
      Bsp::writeLcdByte(*this, 0x20);
    else
      Bsp::writeLcdByte(*this, *p);
  }
}

void
Parallel4Bit::sendCommand(uint8_t command)
{
  Bsp::clearLcdRS(*this);
  Bsp::writeLcdByte(*this, command);
  Bsp::delay(MilliSecond<>(3));
}