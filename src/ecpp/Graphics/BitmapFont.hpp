/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_GRAPHICS_BITMAPFONT_HPP_
#define ECPP_GRAPHICS_BITMAPFONT_HPP_

#include "ecpp/Coordinates.hpp"
#include "ecpp/StringEncodings/Unicode.hpp"
#include <cstdint>

namespace ecpp::Graphics
{
  class BitmapFont
  {
  public:
    template<typename Addr>
    struct Glyph
    {
      Addr                width;
      Addr                height;
      Point<Addr>         render_offset;
      Rect<Point<Addr> >  bitmap_rect;
    };
  };

  template<typename BufferProxy, typename Font>
  class FontRenderer
  {
  protected:
    using Codepoint = ::ecpp::StringEncodings::Unicode::Codepoint;

  public:
    constexpr FontRenderer(BufferProxy buffer_proxy) : buffer_proxy_{buffer_proxy} {}
    constexpr FontRenderer(BufferProxy buffer_proxy, Font font) : buffer_proxy_{buffer_proxy} {}

    FontRenderer & operator<< (Codepoint cp);

    constexpr auto GetWidth() const { return loc_.x; }

  protected:
    BufferProxy                     buffer_proxy_;
    typename BufferProxy::Point     loc_     {0,0};
    Codepoint                       last_cp_ {};
  };

  template<typename BufferProxy, typename Font>
  FontRenderer<BufferProxy, Font> & FontRenderer<BufferProxy, Font>::operator<< (Codepoint cp)
  {
    auto glyph   = Font::GetGlyph(cp);
    auto kerning = Font::GetKerning(last_cp_, cp);
    auto bitmap  = Font::GetTexture().GetSubArea(glyph.bitmap_rect);

    buffer_proxy_.Or(bitmap, loc_.x + kerning + glyph.render_offset.x, loc_.y + glyph.render_offset.y);

    /* TODO: The +1 is probably wrong but ubfg output plus our implementations
     * results in zero space between some (but no all) letters. Not yet sure why,
     * needs more investigation. The +1 ensures the characters can be separated. */ 
    loc_.x += kerning + glyph.width + 1;
    last_cp_ = cp;

    return *this;
  };

}

#endif