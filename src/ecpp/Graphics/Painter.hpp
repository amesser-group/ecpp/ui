/*
 *  Copyright 2021 Andreas Messer <andi@bastelmap.de>
 *
 *  This file is part of the Embedded C++ Platform Project.
 *
 *  Embedded C++ Platform Project (ECPP) is free software: you can
 *  redistribute it and/or modify it under the terms of the GNU General
 *  Public License as published by the Free Software Foundation,
 *  either version 3 of the License, or (at your option) any later
 *  version.
 *
 *  Embedded C++ Platform Project is distributed in the hope that it
 *  will be useful, but WITHOUT ANY WARRANTY; without even the implied
 *  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *  See the GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  As a special exception, the copyright holders of ECPP give you
 *  permission to link ECPP with independent modules to produce an
 *  executable, regardless of the license terms of these independent
 *  modules, and to copy and distribute the resulting executable under
 *  terms of your choice, provided that you also meet, for each linked
 *  independent module, the terms and conditions of the license of that
 *  module.  An independent module is a module which is not derived from
 *  or based on ECPP.  If you modify ECPP, you may extend this exception
 *  to your version of ECPP, but you are not obligated to do so.  If you
 *  do not wish to do so, delete this exception statement from your
 *  version.
 *  */
#ifndef ECPP_GRAPHICS_PAINTER_HPP_
#define ECPP_GRAPHICS_PAINTER_HPP_

#include "ecpp/StringEncodings/Unicode.hpp"
#include "ecpp/Graphics/BitmapFont.hpp"
#include "ecpp/ArrayString.hpp"

#include <cstdarg>
#include <stdio.h>
#include <type_traits>

namespace ecpp::Graphics
{
  /** A dummy frame buffer adapter used to calculate the length of string */
  class NullFrameBufferAdapter
  {
  public:
    using Point = ::ecpp::Point<uint_least8_t>;

    template<typename Source>
    constexpr void Or(const Source& src, uint_least8_t col, uint_least8_t row) {};
  };

  template<typename FrameBufferAdapter>
  class Painter : protected FrameBufferAdapter
  {
  protected:
    using StringPointer = ::ecpp::StringPointer<::ecpp::StringEncodings::Utf8>;
    using StringView = ::ecpp::StringView<::ecpp::StringEncodings::Utf8>;

  public:
    using typename FrameBufferAdapter::Rect;
    using typename FrameBufferAdapter::Point;

    using FrameBufferAdapter::GetWidth;
    using FrameBufferAdapter::GetHeight;
    using FrameBufferAdapter::width;
    using FrameBufferAdapter::height;

    using FrameBufferAdapter::Clear;
    using FrameBufferAdapter::Invert;

    using FrameBufferAdapter::Or;

    constexpr Painter(const FrameBufferAdapter& init) : FrameBufferAdapter{init} {}

    constexpr Painter CreateField(Rect rect) {return {FrameBufferAdapter::GetSubArea(rect)}; }

    template<typename Font>
    void PutText(StringPointer string);

    template<typename Font>
    void PutText(StringView string);

    template<typename Font>
    void CenterText(StringPointer string);

    template<typename Font>
    void CenterText(StringView string);

    template<typename Font>
    void IPrintF(const char* format, ...);

    constexpr Rect GetSize() const { return {{0,0}, {GetWidth(), GetHeight()}}; }

    template<typename Font>
    Rect GetStringRect(StringPointer string) const;

    template<typename Font>
    Rect GetStringRect(StringView string) const;

  protected:
    constexpr FrameBufferAdapter& AsBufferAdapter() { return static_cast<FrameBufferAdapter&>(*this); }

    template<typename Font>
    constexpr auto             CreateFontRenderer() { return Font::CreateRenderer(AsBufferAdapter()); }

    template<typename Font>
    constexpr auto             CreateStringWidthEstimator() const { return Font::CreateRenderer(NullFrameBufferAdapter());}
  };

  template<typename FrameBufferAdapter>
  template<typename Font>
  void Painter<FrameBufferAdapter>::PutText(StringPointer string)
  {
    auto font_renderer = CreateFontRenderer<Font>();

    for(auto it = string.begin(); it < string.end(); ++it)
      font_renderer << *it;
  }

  template<typename FrameBufferAdapter>
  template<typename Font>
  void Painter<FrameBufferAdapter>::PutText(StringView string)
  {
    auto font_renderer = CreateFontRenderer<Font>();

    for(auto it = string.begin(); it < string.end(); ++it)
      font_renderer << *it;
  }

  template<typename FrameBufferAdapter>
  template<typename Font>
  typename Painter<FrameBufferAdapter>::Rect Painter<FrameBufferAdapter>::GetStringRect(StringPointer string) const
  {
    auto estimator = CreateStringWidthEstimator<Font>();

    for(auto it = string.begin(); it < string.end(); ++it)
      estimator << *it;

    return {0, 0, estimator.GetWidth(), Font::GetHeight()};
  }

  template<typename FrameBufferAdapter>
  template<typename Font>
  typename Painter<FrameBufferAdapter>::Rect Painter<FrameBufferAdapter>::GetStringRect(StringView string) const
  {
    auto estimator = CreateStringWidthEstimator<Font>();

    for(auto it = string.begin(); it < string.end(); ++it)
      estimator << *it;

    return {0, 0, estimator.GetWidth(), Font::GetHeight()};
  }

  template<typename FrameBufferAdapter>
  template<typename Font>
  void Painter<FrameBufferAdapter>::CenterText(StringPointer string)
  {
    auto r = GetStringRect<Font>(string);
    auto w = r.GetWidth();

    if (w < GetWidth())
      CreateField( {(GetWidth() - w)/ 2, 0, (GetWidth() + w + 1)/2,Font::GetHeight()}).template PutText<Font>(string);
    else
      PutText<Font>( string );
  }

  template<typename FrameBufferAdapter>
  template<typename Font>
  void Painter<FrameBufferAdapter>::CenterText(StringView string)
  {
    auto r = GetStringRect<Font>(string);
    auto w = r.GetWidth();

    if (w < GetWidth())
      CreateField( {(GetWidth() - w)/ 2, 0, (GetWidth() + w + 1)/2,Font::GetHeight()}).template PutText<Font>(string);
    else
      PutText<Font>( string );
  }


  template<typename FrameBufferAdapter>
  template<typename Font>
  void Painter<FrameBufferAdapter>::IPrintF(const char* format, ...)
  {
    ::ecpp::ArrayString<::ecpp::StringEncodings::Utf8, 128> buffer;
    ::std::va_list args;

    va_start(args, format);
    vsniprintf(buffer.data(), buffer.max_size(), format, args);
    va_end(args);

    PutText<Font>(buffer);
  }

}
#endif 