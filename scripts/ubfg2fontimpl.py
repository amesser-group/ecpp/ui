#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  Copyright 2021 Andreas Messer <andi@bastelmap.de>
#
#  This file is part of the Embedded C++ Platform Project.
#
#  Embedded C++ Platform Project (ECPP) is free software: you can
#  redistribute it and/or modify it under the terms of the GNU General
#  Public License as published by the Free Software Foundation,
#  either version 3 of the License, or (at your option) any later
#  version.
#
#  Embedded C++ Platform Project is distributed in the hope that it
#  will be useful, but WITHOUT ANY WARRANTY; without even the implied
#  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#  See the GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with ECPP.  If not, see <http://www.gnu.org/licenses/>.
import argparse
import os.path
import png
import base64
import xml.etree.ElementTree as ET
from jinja2 import Environment, DictLoader, StrictUndefined


language_templates = {}
language_templates['.hpp'] = """
/* Auto generated C++ Font implementation */
{% if cpp_namespace -%}
#ifndef {{ (cpp_namespace + '_' + header_file)|sanitize|upper }}_
#define {{ (cpp_namespace + '_' + header_file)|sanitize|upper }}_
{%- else %}
#ifndef {{ header_file|sanitize|upper }}_
#define {{ header_file|sanitize|upper }}_
{%- endif %}
#include "ecpp/Graphics/BitmapFont.hpp"
#include "ecpp/StringEncodings/Unicode.hpp"
#include "ecpp/Graphics/Monochrome/PagedBufferProxy.hpp"

{% if cpp_namespace -%}
namespace {{ cpp_namespace }}
{
{%- endif %}
  class {{ basename }}Fonts
  {
  protected:
    using Unicode = ::ecpp::StringEncodings::Unicode;
    using Glyph   = ::ecpp::Graphics::BitmapFont::Glyph<{{ cpp_address_type(fonts) }}>;
    using Texture = ::ecpp::Graphics::Monochrome::PagedBufferProxy<{{ texture.width }}, const uint8_t>;

    static const uint8_t kTexture[{{ texture.width }} * {{ texture.height }} / 8];
  public:
    {%- for font in fonts %}
    class {{ font.safe_name }} : protected ::ecpp::Graphics::BitmapFont
    {
    public:
      template<typename Target>
      static constexpr ::ecpp::Graphics::FontRenderer<Target,{{ font.safe_name }}>  CreateRenderer(Target target) { return { target }; }

      static constexpr Texture          GetTexture() { return { kTexture, 0, 0, {{ texture.width }}, {{ texture.height }} }; }
      static {{ basename }}Fonts::Glyph GetGlyph(Unicode::Codepoint cp);
      static int_fast8_t                GetKerning(Unicode::Codepoint first, Unicode::Codepoint second);
      static constexpr auto             GetHeight() { return {{ font.characters|max(attribute='OrigHeight') |attr('OrigHeight')}}; }
    };
    {%- endfor %}
  };
{% if cpp_namespace -%}
}
{%- endif %}
#endif
"""

language_templates['.cpp'] = """
/* Auto generated C++ Font implementation */
#include "{{ header_file }}"

{% if cpp_namespace -%}
using namespace {{ cpp_namespace }};

{% endif -%}
const uint8_t {{ basename }}Fonts::kTexture[] = {
  {%- for chunk in  texture.paged_row_major(8)|slice(texture.height) %}
  {{ chunk | join(',') }},
  {%- endfor %}
};

{% for font in fonts -%}
{{ basename }}Fonts::Glyph {{ basename }}Fonts::{{ font.safe_name }}::GetGlyph(Unicode::Codepoint cp)
{
  switch(cp.GetRaw())
  {
  {%- for char in font.characters %}
  {% if char.id == unsupported_cp -%}
  default:
  {% endif -%}
  case {{ char.id }}: return { 
      .width         = {{ char.OrigWidth }},
      .height        = {{ char.OrigHeight }},
      .render_offset = { {{ char.Xoffset }}, {{ char.Yoffset }}  },
      .bitmap_rect   = { {{ char.x }}, {{ char.y }}, {{ char.x + char.width }}, {{ char.y + char.height }} }
    };
  {%- endfor %}
  }
}

int_fast8_t {{ basename }}Fonts::{{ font.safe_name }}::GetKerning(Unicode::Codepoint first, Unicode::Codepoint second)
{
  switch(first.GetRaw())
  {
  {%- for first, items in font.kernings|groupby("first") %}
  case {{ first }}:
    switch (second.GetRaw())
    {
    {%- for k in items|sort(attribute='second') %}
    case {{ k.second }}: return {{ k.value }};
    {%- endfor %}
    }
    break;
  {%- endfor %}
  }

  return 0;
}

{% endfor %}
"""

def sanitize_string(s):
    return s.replace(" ","").replace("::","_").replace(".","_")

def cpp_address_type(font_or_fonts):
    fields = "OrigWidth OrigHeight Xoffset Yoffset x y width height".split()

    def get_max_coordinate(font):
        return max(getattr(font,x) for x in fields)

    if isinstance(font_or_fonts, UBFGXMLFontWrapper):
      font_or_fonts = [font_or_fonts]

    max_coordinate = max(get_max_coordinate(c) for font in font_or_fonts for c in font.characters )

    if max_coordinate < 256:
      return "uint_least8_t"
    elif max_coordinate < 256*256:
      return "uint_least16_t"
    else:
      return "uint_least32_t"

class UBFGXMLEntity(object):
    def __init__(self, node):
        self.node = node

class UBFGXMLCharWrapper(UBFGXMLEntity):
    def __getattr__(self, name):
        self.__dict__.update(dict(((k, int(v)) for k,v in self.node.attrib.items())))
        return super(UBFGXMLCharWrapper, self).__getattribute__(name)

class UBFGXMLKerningWrapper(UBFGXMLEntity):
    def __getattr__(self, name):
        self.__dict__.update(dict(((k, int(v)) for k,v in self.node.attrib.items())))
        return super(UBFGXMLKerningWrapper, self).__getattribute__(name)

class UBFGXMLFontWrapper(UBFGXMLEntity):

    @property
    def safe_name(self):
        return sanitize_string(self.node.attrib['name'] + self.node.attrib['size'])

    @property
    def characters(self):
        return sorted((UBFGXMLCharWrapper(char_node) for char_node in self.node.iter('char')), key = lambda x : x.id)

    @property
    def kernings(self):
        return (UBFGXMLKerningWrapper(kerning_node) for kerning_node in self.node.iter('kerning'))


class UBFGXMLFontsWrapper(UBFGXMLEntity):
    def __iter__(self):
        return (UBFGXMLFontWrapper(font_node) for font_node in self.node.iter('font'))


class UBFGXMLTextureWrapper(UBFGXMLEntity):
    @property
    def width(self):
        return int(self.node.attrib['width'])

    @property
    def height(self):
        return int(self.node.attrib['height'])

    @property
    def image_pixels(self):
        raw = base64.b64decode("".join(self.node.itertext()))
        
        width, height, rows, info = png.Reader(bytes=raw).asRGB8()

        return list(rows)


    def paged_row_major(self, bits):
        pixels = self.image_pixels

        for y in range(0, self.height, bits):
            for x in range(0, self.width):
                val = 0
                for i in range(bits):
                    if tuple(pixels[y+i][(3*x):(3*x+3)]) != (0,0,0):
                        val |= 0x1 << i

                yield "0x{:x}".format(val)



def main():
    parser = argparse.ArgumentParser(description=u'Generate Implementation for UBFG generate font bitmap')
    parser.add_argument('--namespace',   default=None, help=u'C++: Place generated classes in given namespace')
    parser.add_argument('--unsupported-cp',  type=int, default=65533, help=u'USe this codepoint if unsupported codepoint requested')

    parser.add_argument('xml_file', type=argparse.FileType("rt"))
    parser.add_argument('header_file', type=argparse.FileType("wt"))
    parser.add_argument('source_file', type=argparse.FileType("wt"))

    args = parser.parse_args()

    root_node = ET.parse(args.xml_file).getroot()

    ctx = {
        "fonts"         : UBFGXMLFontsWrapper(root_node),
        "texture"       : UBFGXMLTextureWrapper(root_node.find('texture')),
        "unsupported_cp": args.unsupported_cp,
        "basename"      : os.path.basename(os.path.splitext(args.xml_file.name)[0]),
        "header_file"   : args.header_file.name,
        "source_file"   : args.source_file.name,
        "cpp_namespace" : args.namespace,
        "cpp_address_type" : cpp_address_type,
    }

    env = Environment(
        loader = DictLoader(language_templates),
        undefined = StrictUndefined
    )

    env.filters['sanitize'] = sanitize_string

    args.header_file.write(env.get_template('.hpp').render(ctx))
    args.header_file.close()

    args.source_file.write(env.get_template('.cpp').render(ctx))
    args.source_file.close()

if __name__ == '__main__':
    main()