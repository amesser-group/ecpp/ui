#! /usr/bin/env python
# vim: set fileencoding=utf-8 ts=4 sw=4 et

def configure(conf):
    f = conf.env.append_value
    f('INCLUDES',  [ conf.path.find_dir('src').abspath() ])

def build(bld):

    bld.ecpp_build(
        target   = 'ecpp-ui',
        source   = bld.path.find_dir('src/ecpp/Drivers').ant_glob('**/*.cpp'),
        features = 'cxx cstlib',
    )
